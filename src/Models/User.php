<?php

namespace TCG\Voyager\Models;

use App\Models\Student;
use App\Models\Supervisor;
use App\Models\User_Role;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\VoyagerUser;

class User extends AuthUser
{
    use VoyagerUser;

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('F jS, Y h:i A');
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function hasRole($role)
    {
        $c = Student::where('user_id', '=', $this->id)->count();
        if ($c > 0) {
            return false;
        }
        return true;
    }

    public static function isAdmin()
    {
        $stc = Student::where('user_id', '=', Auth::user()->id)->count();
        $spc = Supervisor::where('user_id', '=', Auth::user()->id)->count();
        if ($stc == 0 && $spc == 0) {
            return true;
        }
        return false;
    }
}
